    
    var getList = function(){
        var scQuery = document.getElementById('scQuery').value;
        var requestParams = 'tracks.json?' + config.clientId + '&q=' + scQuery + '&limit=20';
                
        getResponse(requestParams, scList);
    }

    var getDetail = function(){
            
        var requestParams = 'tracks/' + getId() + '.json?' + config.clientId;
        
        getResponse(requestParams, scDetail);
        
    }

  
    var getResponse = function(params, func){
            
        var xhr = new XMLHttpRequest();
        var url = 'http://api.soundcloud.com/' + params;
        
        xhr.open('GET', url, true);

        xhr.onreadystatechange= function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                var response = JSON.parse(xhr.responseText);
                                
                func(response);
            }
        };
        
        xhr.send();
    }

    function scList(arr) {
        var out = '';
        var i;
        for(i = 0; i < arr.length; i++) {
            out += '<li><a href="detail.html?id=' + arr[i].id + '"><img src="' + arr[i].artwork_url + '"></a></li>';
        }
        
            
        document.getElementById('list').innerHTML = out;
    }
  
    function scDetail(arr) {
        
        var img = document.createElement('img');
        img.src = arr.artwork_url;
        document.getElementById('trackImg').appendChild(img);
        document.getElementById('trackTitle').innerHTML = arr.title;
        document.getElementById('trackDesc').innerHTML = arr.description;
        document.getElementById('trackDL').setAttribute('href', 'http://api.soundcloud.com/tracks/' + getId() + '/download?' + config.clientId);          
    }

    function getId() {
           var query = window.location.search.substring(1);
           var vars = query.split("&");
           for (var i=0; i < vars.length; i++) {
                   var pair = vars[i].split("=");
                   return pair[1];
           }
           console.log(query);
           return(false);
    }